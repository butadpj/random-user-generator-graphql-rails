module Types
  class QueryType < Types::BaseObject
    field :users, [Types::UserType], null: true do
      description 'Query all Users'
    end

    field :random_user, Types::UserType, null: true do
      description 'Query a random User'
    end

    field :current_user, Types::UserType, null: true do
      description 'Query the currently logged in User'
    end

    def users
      User.all
    end

    def random_user
      if User.any?
        user_ids = User.all.ids
        random_id = user_ids.sample
        User.find(random_id)
      end
    end

    def current_user
      context[:current_user]
    end
  end
end
