module Mutations
  class LogoutUser < BaseMutation
    field :message, String, null: false

    def resolve
      @message = ''
      if context[:current_user]
        context[:session][:token] = ''
        @message = 'User has successfuly logged out'
      else
        @message = "There's no currently logged in user"
      end
      { message: @message }
    end
  end
end
