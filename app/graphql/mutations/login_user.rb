module Mutations
  class LoginUser < BaseMutation
    field :user, Types::UserType, null: false
    field :token, String, null: true
    field :message, String, null: true

    argument :credentials, Types::AuthProviderCredentialsInput, required: true

    def resolve(credentials:)
      return unless credentials

      @user = User.find_by(username: credentials[:username])

      return unless @user
      return unless @user.authenticate(credentials[:password])

      @token = AuthToken.token_for_user(@user)

      context[:session][:token] = @token
      @message = 'Successfuly logged in!'

      { user: @user, token: @token, message: @message }
    end
  end
end
